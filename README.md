# Simple Arduino Digital Clock

This is the simplest possible Arduino Digital Clock. It only uses an Arduino Nano board, 7-Segment Leds, and Two Switches to set the time.
Accuracy depends on the Arduino 16Mhz crystal, but will be no less accurate than your average home clock.

![](schematic/schematic.png) 

Blog post with full instructions [here](https://blog.johannfenech.com/a-zero-component-super-simple-arduino-digital-clock/)

