int hh, mm, ss; //declared as globals
boolean toggle = 0;

#include "includes/definitions.h";
#include "includes/setup.h";
#include "includes/display_subroutines.h";


void loop() {
  
  while (1){

    //check if the SET_H button is pressed, and if it is, clear display, and increment hours
    if (!digitalRead (SET_H)){
      hh++;
      if (hh > 23){
        hh = 0;
      }
      while (!digitalRead (SET_H)){
        digitalWrite (SEG_A, L);
        digitalWrite (SEG_B, L); 
        digitalWrite (SEG_C, L);
        digitalWrite (SEG_D, L);
        digitalWrite (SEG_E, L);
        digitalWrite (SEG_F, L);
        digitalWrite (SEG_G, L);
      }
    }

    //check if the SET_M button is pressed, and if it is, clear display, and increment minutes
    if (!digitalRead (SET_M)){
      mm++;
      if (mm > 59){
        mm = 0;
      }
      while (!digitalRead (SET_M)){
        digitalWrite (SEG_A, L);
        digitalWrite (SEG_B, L); 
        digitalWrite (SEG_C, L);
        digitalWrite (SEG_D, L);
        digitalWrite (SEG_E, L);
        digitalWrite (SEG_F, L);
        digitalWrite (SEG_G, L);
      }
    }


    //call the display time subroutine
    displayTime (hh,mm);
  }

}




