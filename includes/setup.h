//Setup IO Ports

void setup() {
  cli(); // disable all interrupts

  //set timer1 interrupt at 1Hz
  TCCR1A = 0;// set entire TCCR1A register to 0
  TCCR1B = 0;// same for TCCR1B
  TCNT1  = 0;//initialize counter value to 0
  // set compare match register for 1hz increments
  OCR1A = 15624;// = (16*10^6) / (1*1024) - 1 (must be <65536)
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS10 and CS12 bits for 1024 prescaler
  TCCR1B |= (1 << CS12) | (1 << CS10);  
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);

  sei();//enable interrupts


  //set digit and segment pins to outputs
  pinMode (DIGIT_1, OUTPUT);
  pinMode (DIGIT_2, OUTPUT);
  pinMode (DIGIT_3, OUTPUT);
  pinMode (DIGIT_4, OUTPUT);

  pinMode (SEG_A, OUTPUT);
  pinMode (SEG_B, OUTPUT);
  pinMode (SEG_C, OUTPUT);
  pinMode (SEG_D, OUTPUT);
  pinMode (SEG_E, OUTPUT);
  pinMode (SEG_F, OUTPUT);
  pinMode (SEG_G, OUTPUT);

  //set button pins to input
  pinMode (SET_H, INPUT);
  pinMode (SET_M, INPUT);

  //and set internal pullups
  digitalWrite (SET_H, H);
  digitalWrite (SET_M, H);
  
  //turn off all digits
  digitalWrite (DIGIT_1, H);
  digitalWrite (DIGIT_2, H);
  digitalWrite (DIGIT_3, H);
  digitalWrite (DIGIT_4, H);
}

//Interrupt service routine. This gets trigerred exactly every one second
ISR(TIMER1_COMPA_vect){//timer1 interrupt 1Hz toggles pin 13 (LED)
   // Toggle Second marker LED
  if (toggle){
    digitalWrite(SEC_LED, H);
    toggle = 0;
  }
  else{
    digitalWrite(SEC_LED, L);
    toggle = 1;
  }

// Increment seconds, if seconds = 60, set seconds to 0, and increment minutes
  ss++;
  if (ss > 59){
    ss=0;
    mm ++;
  }

// If minutes = 59, set minutes = 0, and increment hours
  if (mm > 59){
    mm = 0;
    hh++;
  }
  
// if hours = 24, set hours to 0  
  if (hh > 23){
    hh = 0;    
  }

}