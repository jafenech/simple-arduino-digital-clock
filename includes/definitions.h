//Pin definitions, self explanatory

//Common Cathode pins
#define DIGIT_1 A0
#define DIGIT_2 A1
#define DIGIT_3 A2
#define DIGIT_4 A3

//7-Segment pins
#define SEG_A A4
#define SEG_B A5
#define SEG_C 0x02
#define SEG_D 0x03
#define SEG_E 0x04
#define SEG_F 0x05
#define SEG_G 0x06

//Set H and M buttons
#define SET_H 0x07
#define SET_M 0x08

//Second LED
#define SEC_LED 13

//Generic definitions used internally
#define H HIGH
#define L LOW