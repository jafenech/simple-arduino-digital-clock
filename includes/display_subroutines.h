
//The setDigit subroutine, selectes the correct 7-Segment digit where the respective number
//will be displayed (using showNumber)
int setDigit (int digit){
  if (digit == 1){
    digitalWrite (A0, L);
    digitalWrite (A1, H);
    digitalWrite (A2, H);
    digitalWrite (A3, H);
  }

  if (digit == 2){
    digitalWrite (A0, H);
    digitalWrite (A1, L);
    digitalWrite (A2, H);
    digitalWrite (A3, H);
  }

  if (digit == 3){
    digitalWrite (A0, H);
    digitalWrite (A1, H);
    digitalWrite (A2, L);
    digitalWrite (A3, H);
  }

  if (digit == 4){
    digitalWrite (A0, H);
    digitalWrite (A1, H);
    digitalWrite (A2, H);
    digitalWrite (A3, L);
  }
}

//The showNumber subroutine, converts the number value, into 7-Segment 
int showNumber (int number){

  if (number == 1){
      digitalWrite (SEG_A, L);
      digitalWrite (SEG_B, H); 
      digitalWrite (SEG_C, H);
      digitalWrite (SEG_D, L);
      digitalWrite (SEG_E, L);
      digitalWrite (SEG_F, L);
      digitalWrite (SEG_G, L);      
      delay(5);
  }

  if (number == 2){
      digitalWrite (SEG_A, H);
      digitalWrite (SEG_B, H); 
      digitalWrite (SEG_C, L);
      digitalWrite (SEG_D, H);
      digitalWrite (SEG_E, H);
      digitalWrite (SEG_F, L);
      digitalWrite (SEG_G, H);
      delay(5);
  }

  if (number == 3){
      digitalWrite (SEG_A, H);
      digitalWrite (SEG_B, H); 
      digitalWrite (SEG_C, H);
      digitalWrite (SEG_D, H);
      digitalWrite (SEG_E, L);
      digitalWrite (SEG_F, L);
      digitalWrite (SEG_G, H);
      delay(5);
  }

  if (number == 4){
      digitalWrite (SEG_A, L);
      digitalWrite (SEG_B, H); 
      digitalWrite (SEG_C, H);
      digitalWrite (SEG_D, L);
      digitalWrite (SEG_E, L);
      digitalWrite (SEG_F, H);
      digitalWrite (SEG_G, H);
      delay(5);
  }

  if (number == 5){
      digitalWrite (SEG_A, H);
      digitalWrite (SEG_B, L); 
      digitalWrite (SEG_C, H);
      digitalWrite (SEG_D, H);
      digitalWrite (SEG_E, L);
      digitalWrite (SEG_F, H);
      digitalWrite (SEG_G, H);
      delay(5);  
      }

  if (number == 6){
      digitalWrite (SEG_A, L);
      digitalWrite (SEG_B, L); 
      digitalWrite (SEG_C, H);
      digitalWrite (SEG_D, H);
      digitalWrite (SEG_E, H);
      digitalWrite (SEG_F, H);
      digitalWrite (SEG_G, H);
      delay(5);  
      }

  if (number == 7){
      digitalWrite (SEG_A, H);
      digitalWrite (SEG_B, H); 
      digitalWrite (SEG_C, H);
      digitalWrite (SEG_D, L);
      digitalWrite (SEG_E, L);
      digitalWrite (SEG_F, L);
      digitalWrite (SEG_G, L);
      delay(5);  
  }

  if (number == 8){
      digitalWrite (SEG_A, H);
      digitalWrite (SEG_B, H); 
      digitalWrite (SEG_C, H);
      digitalWrite (SEG_D, H);
      digitalWrite (SEG_E, H);
      digitalWrite (SEG_F, H);
      digitalWrite (SEG_G, H);
      delay(5);
  }

  if (number == 9){
      digitalWrite (SEG_A, H);
      digitalWrite (SEG_B, H); 
      digitalWrite (SEG_C, H);
      digitalWrite (SEG_D, H);
      digitalWrite (SEG_E, L);
      digitalWrite (SEG_F, H);
      digitalWrite (SEG_G, H);
      delay(5);
  }

  if (number == 0){
      digitalWrite (SEG_A, H);
      digitalWrite (SEG_B, H); 
      digitalWrite (SEG_C, H);
      digitalWrite (SEG_D, H);
      digitalWrite (SEG_E, H);
      digitalWrite (SEG_F, H);
      digitalWrite (SEG_G, L);
      delay(5);
  }
}

//The displayNumber subroutine, selects the correct digit, before calling the showNumber
//subroutine
int displayNumber (int digit, int number){
  setDigit(digit);
  showNumber(number);
}


//The display time subroutine, converts Hours to h1, h2 and Minutes into m1 and m2, 
//then it calls the displayNumber subroutine accordingly
int displayTime (int hh, int mm){
    int h1, h2, m1, m2;

    //Split hh into two separate digits (h1 and h2)
    h1 = (hh / 10);
    h2 = (hh - h1*10);
    
    //Split mm into two separate digits (m1 and m2)
    m1 = (mm / 10);
    m2 = (mm - m1*10);

    //display h1 as digit 1, h2 as digit 2, m1 as digit 3, m4 as digit 4            
    displayNumber (1, h1);
    displayNumber (2, h2);
    displayNumber (3, m1);
    displayNumber (4, m2);
}